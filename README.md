Configuració inicial

Git global setup

git config --global user.name "MisionTIC_UIS_GACU"
git config --global user.email "gacu517@gmail.com"

Create a new repository

git clone https://gitlab.com/s04g13-tiendavirtual1/poyectoiniciadopc.git
cd poyectoiniciadopc
git switch -c main
touch README.md
git add README.md
git commit -m "add README"
git push -u origin main

Push an existing folder

cd existing_folder
git init --initial-branch=main
git remote add origin https://gitlab.com/s04g13-tiendavirtual1/poyectoiniciadopc.git
git add .
git commit -m "Initial commit"
git push -u origin main

Push an existing Git repository

cd existing_repo
git remote rename origin old-origin
git remote add origin https://gitlab.com/s04g13-tiendavirtual1/poyectoiniciadopc.git
git push -u origin --all
git push -u origin --tags

